package de.lars.wolfram.ttproblem.evolution.crossover;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;
import de.lars.wolfram.ttproblem.evolution.rand.Randomizer;
import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.Person;

import java.util.List;

/**
 * @author wolframl
 */
public class NormalCrossover implements Crossover
{
    private final Randomizer randomGenerator;

    public NormalCrossover(Randomizer randomGenerator)
    {
        this.randomGenerator = randomGenerator;
    }

    @Override public IndividualPart create(IndividualPart a, IndividualPart b)
    {
        IndividualPart child = a.copy();
        for (Room room : child.getRooms())
        {
            if (randomGenerator.nextBoolean())
            {
                List<Person> persons = b.getPersons(room);
                child.setPersons(persons, room);
            }
        }
        return child;
    }

}

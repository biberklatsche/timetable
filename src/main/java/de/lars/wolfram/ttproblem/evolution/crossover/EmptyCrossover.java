package de.lars.wolfram.ttproblem.evolution.crossover;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;

/**
 * Created by lwo on 02.05.2016.
 */
public class EmptyCrossover implements Crossover
{
    @Override public IndividualPart create(IndividualPart a, IndividualPart b)
    {
        return a;
    }
}

package de.lars.wolfram.ttproblem.evolution.rand;

import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.FixedPerson;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;
import de.lars.wolfram.ttproblem.plan.timetable.TimetableKey;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author wolframl
 */
public class Randomizer
{
    private final static Random randomGenerator = new Random(System.currentTimeMillis());
    private final List<Person>             persons;
    private final List<? extends TimeSlot> times;
    private final List<Room>               rooms;

    public Randomizer(List<Person> persons, List<? extends TimeSlot> times, List<Room> rooms)
    {
        this.persons = persons.stream()
                .filter(person -> !person.getClass().isAnnotationPresent(FixedPerson.class))
                .collect(Collectors.toList());
        this.times = times;
        this.rooms = rooms;
    }

    public Person anyPerson()
    {
        int index = randomGenerator.nextInt(persons.size());
        Person person = persons.get(index);
        return person;
    }

    public Person anyPerson(List<Person> persons)
    {
        List<Person> personsWithoutFixed = persons.stream()
                .filter(person -> !person.getClass().isAnnotationPresent(FixedPerson.class))
                .collect(Collectors.toList());
        int index = randomGenerator.nextInt(personsWithoutFixed.size());
        return personsWithoutFixed.get(index);
    }

    public TimetableKey anyTimetableKey()
    {
        int indexTimes = randomGenerator.nextInt(times.size());
        TimeSlot time = times.get(indexTimes);
        int indexRooms = randomGenerator.nextInt(rooms.size());
        Room room = rooms.get(indexRooms);
        return new TimetableKey(time, room);
    }

    public Boolean nextBoolean()
    {
        return randomGenerator.nextBoolean();
    }

    public Double nextDouble()
    {
        return randomGenerator.nextDouble();
    }

    public Integer nextInteger(int bound)
    {
        return randomGenerator.nextInt(bound);
    }
}

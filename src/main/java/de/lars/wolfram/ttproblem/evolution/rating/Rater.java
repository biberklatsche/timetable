package de.lars.wolfram.ttproblem.evolution.rating;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;

/**
 * @author wolframl
 */
public interface Rater
{
    Double rate(IndividualPart individualPart);
}

package de.lars.wolfram.ttproblem;

import de.lars.wolfram.ttproblem.evolution.World;

import java.io.IOException;

public class MainClass
{
    public static void main(String[] args) throws IOException {
        World world = new World();

        world.run();
    }

}
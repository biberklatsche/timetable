package de.lars.wolfram.ttproblem.plan.timetable;

import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.FixedPerson;
import de.lars.wolfram.ttproblem.plan.persons.Free;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

/**
 * @author wolframl
 */
public abstract class TimetablePart
{
    protected final SortedMap<Room, Integer> roomCoordinates;

    protected final SortedMap<TimeSlot, Integer> timeCoordinates;

    protected final Person[][] table;

    TimetablePart(SortedMap<Room, Integer> roomCoordinates, SortedMap<TimeSlot, Integer> timeCoordinates,
            Person[][] table)
    {
        this.roomCoordinates = roomCoordinates;
        this.timeCoordinates = timeCoordinates;
        this.table = table;
    }

    public void addAllFreeRooms(List<TimetableKey> morningFreeRooms)
    {
        for (TimetableKey key : morningFreeRooms)
        {
            setPerson(key, new Free());
        }
    }

    public abstract TimetablePart copy();

    public int countRooms()
    {
        return roomCoordinates.size();
    }

    public int countTimeSlots()
    {
        return timeCoordinates.size();
    }

    public void fillColumn(Room room, List<Person> persons)
    {
        int counter = 0;
        for (TimeSlot slot : timeCoordinates.keySet())
        {
            if (counter < persons.size())
            {
                TimetableKey key = new TimetableKey(slot, room);
                setPerson(key, persons.get(counter));
                counter++;
            }
            else
            {
                break;
            }
        }
    }

    public Person getPerson(TimetableKey key)
    {
        int quarterCoordinate = timeCoordinates.get(key.getTime());
        int roomCoordinate = roomCoordinates.get(key.getRoom());
        return table[quarterCoordinate][roomCoordinate];
    }

    public List<Person> getPersons(Room room)
    {
        List<Person> persons = new ArrayList<>(timeCoordinates.size());
        for (TimeSlot time : timeCoordinates.keySet())
        {
            persons.add(getPerson(new TimetableKey(time, room)));
        }
        return persons;
    }

    public List<Person> getPersons(TimeSlot time)
    {
        List<Person> persons = new ArrayList<>(roomCoordinates.size());
        for (Room room : roomCoordinates.keySet())
        {
            persons.add(getPerson(new TimetableKey(time, room)));
        }
        return persons;
    }

    public List<TimetableKey> getReplaceableKeys()
    {
        List<TimetableKey> keys = new ArrayList<>(timeCoordinates.size() * roomCoordinates.size());
        for (TimeSlot time : timeCoordinates.keySet())
        {
            for (Room room : roomCoordinates.keySet())
            {
                TimetableKey key = new TimetableKey(time, room);
                if (isReplaceable(key))
                {
                    keys.add(key);
                }
            }
        }
        return keys;
    }

    public Set<Room> getRooms()
    {
        return roomCoordinates.keySet();
    }

    public Set<TimeSlot> getTimeSlots()
    {
        return timeCoordinates.keySet();
    }

    public void setPerson(TimetableKey key, Person person)
    {
        if (isReplaceable(key))
        {
            int timeCoordinate = timeCoordinates.get(key.getTime());
            int roomCoordinate = roomCoordinates.get(key.getRoom());
            table[timeCoordinate][roomCoordinate] = person;
        }
    }

    @Override

    public String toString()
    {
        String tableAsString = printHeader();
        tableAsString += "\n";
        tableAsString += printRows();
        return tableAsString;
    }

    private boolean isReplaceable(TimetableKey key)
    {
        Person person = getPerson(key);
        return person == null || !person.getClass().isAnnotationPresent(FixedPerson.class);
    }

    private String printHeader()
    {
        String header = "\t\tZeit";
        int count = 0;
        for (TimeSlot time : timeCoordinates.keySet())
        {
            header += "\t";
            if (count > 0)
            {
                header += "\t";
            }
            header += time.getFrom();
            count++;
        }
        header += "\n\tRaum";
        return header;
    }

    private String printRows()
    {
        String rows = "";
        for (Room room : roomCoordinates.keySet())
        {
            rows += "\t" + room.getName();
            int count = 0;
            for (TimeSlot time : timeCoordinates.keySet())
            {
                String personName = getPerson(new TimetableKey(time, room)).getName();
                personName = personName.length() > 8 ? personName.substring(0, 8) : personName;
                if (count == 0)
                {
                    rows += "\t\t\t";
                }
                else
                {
                    rows += personName.length() < 4 ? "\t\t\t" : personName.length() < 8 ? "\t\t" : "\t";
                }
                rows += personName;

                count++;
            }
            rows += "\n";
        }
        return rows;
    }
}

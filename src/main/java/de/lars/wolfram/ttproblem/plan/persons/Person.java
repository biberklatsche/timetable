package de.lars.wolfram.ttproblem.plan.persons;

import java.time.LocalTime;
import java.util.Comparator;

/**
 * @author wolframl
 */
public interface Person
{
    Comparator<Person> NAME_COMPARATOR = new Comparator<Person>()
    {
        @Override public int compare(Person o1, Person o2)
        {
            return o1.getName().compareTo(o2.getName());
        }
    };

    LocalTime getEndWorking();

    String getName();

    LocalTime getStartWorking();
}

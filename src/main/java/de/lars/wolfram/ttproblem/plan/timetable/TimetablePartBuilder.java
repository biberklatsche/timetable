package de.lars.wolfram.ttproblem.plan.timetable;

import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.Period;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;

import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author wolframl
 */
public class TimetablePartBuilder
{
    private Period period;
    private List<TimetableKey> morningFreeRooms = new LinkedList<>();
    private List<Room> rooms;

    public TimetablePartBuilder addMorningFreeRoom(TimetableKey key)
    {
        morningFreeRooms.add(key);
        return this;
    }

    public Timetable build()
    {
        SortedMap<TimeSlot, Integer> timeCoordinates = new TreeMap<>(
                (o1, o2) -> o1.getFrom().compareTo(o2.getFrom()));
        int counter = 0;
        for (TimeSlot time : period.getQuarters())
        {
            timeCoordinates.put(time, counter++);
        }

        SortedMap<Room, Integer> roomCoordinates = new TreeMap<>(
                (o1, o2) -> o1.getIndex().compareTo(o2.getIndex()));
        counter = 0;
        for (Room room : rooms)
        {
            roomCoordinates.put(room, counter++);
        }

        Morning morning = new Morning(roomCoordinates, timeCoordinates,
                new Person[timeCoordinates.size()][roomCoordinates.size()]);
        morning.addAllFreeRooms(morningFreeRooms);
        return new Timetable(morning);
    }

    public TimetablePartBuilder period(Period period)
    {
        this.period = period;
        return this;
    }

    public TimetablePartBuilder rooms(List<Room> rooms)
    {
        this.rooms = rooms;
        return this;
    }

    public static TimetablePartBuilder newBuilder()
    {
        TimetablePartBuilder builder = new TimetablePartBuilder();
        return builder;
    }
}

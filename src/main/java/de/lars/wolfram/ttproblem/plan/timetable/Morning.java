package de.lars.wolfram.ttproblem.plan.timetable;

import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;

import java.util.SortedMap;

/**
 * @author wolframl
 */
public class Morning extends TimetablePart
{
    public Morning(SortedMap<Room, Integer> roomCoordinates, SortedMap<TimeSlot, Integer> timeCoordinates,
            Person[][] table)
    {
        super(roomCoordinates, timeCoordinates, table);
    }

    public Morning copy()
    {
        Person[][] newTable = new Person[table.length][table[0].length];
        for (int i = 0; i < table.length; i++)
        {
            for (int j = 0; j < table[0].length; j++)
            {
                newTable[i][j] = table[i][j];
            }
        }
        return new Morning(this.roomCoordinates, this.timeCoordinates, newTable);
    }
}

package de.lars.wolfram.ttproblem.plan.time;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

/**
 * @author wolframl
 */
public class Quarter implements TimeSlot
{
    private final LocalTime from;
    private final LocalTime to;

    public Quarter(LocalTime from)
    {
        this.from = from;
        this.to = from.plus(15, ChronoUnit.MINUTES);
    }

    @Override public LocalTime getFrom()
    {
        return from;
    }

    @Override public LocalTime getTo()
    {
        return to;
    }
}

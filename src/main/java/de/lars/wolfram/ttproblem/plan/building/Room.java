package de.lars.wolfram.ttproblem.plan.building;

/**
 * @author wolframl
 */
public class Room
{
    private final String name;

    private final int index;

    public Room(int index, String name)
    {
        this.name = name;
        this.index = index;
    }

    public Integer getIndex()
    {
        return index;
    }

    public String getName()
    {
        return name;
    }
}

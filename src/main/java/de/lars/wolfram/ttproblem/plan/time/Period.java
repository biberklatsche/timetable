package de.lars.wolfram.ttproblem.plan.time;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

/**
 * @author wolframl
 */
public class Period
{
    final LocalTime from;
    final LocalTime to;

    public Period(LocalTime from, LocalTime to)
    {
        this.from = from;
        this.to = to;
    }

    public LocalTime getFrom()
    {
        return from;
    }

    public List<Quarter> getQuarters()
    {
        List<Quarter> quarters = new LinkedList<>();
        LocalTime current = from;
        while (current.isBefore(to))
        {
            quarters.add(new Quarter(current));
            current = current.plus(15, ChronoUnit.MINUTES);
        }
        return quarters;
    }

    public LocalTime getTo()
    {
        return to;
    }
}

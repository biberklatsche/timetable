package de.lars.wolfram.ttproblem.plan.timetable;

import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;

/**
 * @author wolframl
 */
public class TimetableKey
{
    private final TimeSlot time;

    private final Room room;

    public TimetableKey(TimeSlot time, Room room)
    {
        this.time = time;
        this.room = room;
    }

    public Room getRoom()
    {
        return room;
    }

    public TimeSlot getTime()
    {
        return time;
    }
}
